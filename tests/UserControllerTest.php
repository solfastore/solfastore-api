<?php

class UserControllerTest extends TestCase
{
    public function login()
    {
        $user = factory('App\User')->create([
            'role' => 2,
            'state' => 1
        ]);
        $this->actingAs($user);
        return $user;
    }

    public function testRegisterNewUser()
    {
        $params = [
            'name' => 'New User',
            'email' => 'newuser@mail.com',
            'password' => '12345'
        ];
        $response = $this->call('POST', 'v1/users/register', $params);
        $data = json_decode($response->getContent());

        $this->assertEquals(201, $response->status());
        $this->assertEquals(false, $data->error);
        $this->assertEquals('User has been created.', $data->message);
        $this->seeInDatabase('users', ['name' => 'New User', 'email' => 'newuser@mail.com', 'role' => 2, 'state' => 1]);
    }

    public function testRegisterExistingUser()
    {
        $user = factory('App\User')->create([
            'password' => '12345'
        ]);
        $params = [
            'name' => $user->name,
            'email' => $user->email,
            'password' => '12345'
        ];
        $response = $this->call('POST', 'v1/users/register', $params);
        $data = json_decode($response->getContent());

        $this->assertArrayHasKey('error', (array)$data);
        $this->assertArrayHasKey('message', (array)$data);
        $this->assertArrayHasKey('validation', (array)$data);
        $this->assertEquals(400, $response->status());
        $this->assertEquals(true, $data->error);
        $this->assertEquals('Validation error.', $data->message);
        $this->assertEquals(['email' => ['The email has already been taken.']], (array)$data->validation);
    }

    public function testLoginUser()
    {
        $user = factory('App\User')->create([
            'password' => '12345'
        ]);
        $params = [
            'grant_type' => 'password',
            'client_id' => 2,
            'client_secret' => 'jtENSd6fEJ5k9PFoGHTfWHc5Kqn8i8IzG4fp1DHm',
            'username' => $user->email,
            'password' => '12345',
        ];
        $response = $this->call('POST', 'v1/oauth/token', $params);
        $data = json_decode($response->getContent());
        $this->assertArrayHasKey('access_token', (array)$data);
        $this->assertArrayHasKey('token_type', (array)$data);
        $this->assertArrayHasKey('expires_in', (array)$data);
        $this->assertArrayHasKey('refresh_token', (array)$data);
    }

    public function testLoginUnregisteredUser()
    {
        $params = [
            'grant_type' => 'password',
            'client_id' => 2,
            'client_secret' => 'jtENSd6fEJ5k9PFoGHTfWHc5Kqn8i8IzG4fp1DHm',
            'username' => 'wtfemailisthis@mail.com',
            'password' => '12345',
        ];
        $response = $this->call('POST', 'v1/oauth/token', $params);
        $data = json_decode($response->getContent());

        $this->assertArrayHasKey('error', (array)$data);
        $this->assertArrayHasKey('message', (array)$data);
        $this->assertEquals('invalid_credentials', $data->error);
        $this->assertEquals('The user credentials were incorrect.', $data->message);
    }

    public function testGetUserProfile()
    {
        $this->login();
        $response = $this->call('GET', 'v1/users/profile');
        $data = json_decode($response->getContent());

        $this->assertArrayHasKey('error', (array)$data);
        $this->assertArrayHasKey('message', (array)$data);
        $this->assertArrayHasKey('user', (array)$data);
        $this->assertEquals(false, $data->error);
        $this->assertEquals('Successfully get user profile.', $data->message);
    }

    public function testGetProfileWithGuestCredentials()
    {
        $response = $this->call('GET', 'v1/users/profile');
        $data = json_decode($response->getContent());

        $this->assertArrayHasKey('error', (array)$data);
        $this->assertArrayHasKey('message', (array)$data);
        $this->assertEquals(401, $response->status());
        $this->assertEquals(true, $data->error);
        $this->assertEquals('Unauthorized.', $data->message);
    }

    public function testUpdateProfile()
    {
        $user = $this->login();
        $params = [
            'name' => 'Name Updated'
        ];
        $response = $this->call('POST', 'v1/users/profile/update', $params);
        $data = json_decode($response->getContent());

        $this->assertArrayHasKey('error', (array)$data);
        $this->assertArrayHasKey('message', (array)$data);
        $this->assertEquals(200, $response->status());
        $this->assertEquals(false, $data->error);
        $this->assertEquals('User profile has been updated.', $data->message);
        $this->seeInDatabase('users', ['id' => $user->id, 'name' => 'Name Updated']);
    }
}