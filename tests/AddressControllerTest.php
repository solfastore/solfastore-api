<?php

class AddressControllerTest extends TestCase
{
    /**
     * Logged in customer
     * @return void
     */
    public function customerLogin()
    {
        $user = factory('App\User')->create([
            'role' => 2,
            'state' => 1
        ]);
        $this->actingAs($user);
    }

    /**
     * Create two new address
     * @return void
     */
    public function createNewAddress()
    {
        $this->customerLogin();
        $params = [
            'name' => 'New Customer Address',
            'email' => 'emailforaddress@mail.com',
            'phone' => '081337834312',
            'address' => 'Winterfell, The Old Gods of the Forest.',
            'subdistrict_id' => 1
        ];
        $this->call('POST', '/v1/users/addresses/add', $params);

        $params = [
            'name' => 'Another Customer Address',
            'email' => 'anotheremailforaddress@mail.com',
            'phone' => '081337834312',
            'address' => 'Winterfell, The Old Gods of the Forest.',
            'subdistrict_id' => 1
        ];
        $this->call('POST', '/v1/users/addresses/add', $params);
    }

    /**
     * get all addresses
     * @return array
     */
    public function getAllAddresses()
    {
        $this->createNewAddress();

        $response = $this->call('GET', '/v1/users/addresses');
        $data = json_decode($response->getContent());

        return $data->addresses;
    }

    /**
     * Test create a new address
     * @return void
     */
    public function testCreateNewAddress()
    {
        $this->customerLogin();
        $params = [
            'name' => 'New Customer Address',
            'email' => 'anotheremailforaddress@mail.com',
            'phone' => '081337834312',
            'address' => 'Winterfell, The Old Gods of the Forest.',
            'subdistrict_id' => 1
        ];
        $response = $this->call('POST', '/v1/users/addresses/add', $params);
        $data = json_decode($response->getContent());

        $this->assertEquals(201, $response->status());
        $this->assertEquals(false, $data->error);
        $this->assertEquals('Address has been created.', $data->message);
        $this->seeInDatabase('addresses', $params);
    }

    /**
     * Test create address with not complete field
     * @return void
     */
    public function testCreateAddressWithNotCompleteField()
    {
        $this->customerLogin();
        $params = [
            'name' => 'New Customer Address',
            'email' => 'anotheremailforaddress@mail.com',
            'phone' => '081337834312'
        ];
        $response = $this->call('POST', '/v1/users/addresses/add', $params);
        $data = json_decode($response->getContent());

        $this->assertEquals(202, $response->status());
        $this->assertEquals(true, $data->error);
        $this->assertEquals('Error when trying to create address.', $data->message);
        $this->assertArrayHasKey('log', (array)$data);
    }

    /**
     * Test guest create new address
     * @return void
     */
    public function testGuestCreateNewAddress()
    {
        $params = [
            'name' => 'New Customer Address',
            'email' => 'anotheremailforaddress@mail.com',
            'phone' => '081337834312',
            'address' => 'Winterfell, The Old Gods of the Forest.',
            'subdistrict_id' => 1
        ];
        $response = $this->call('POST', '/v1/users/addresses/add', $params);
        $data = json_decode($response->getContent());

        $this->assertEquals(401, $response->status());
        $this->assertEquals(true, $data->error);
        $this->assertEquals('Unauthorized.', $data->message);
    }

    /**
     * Test get all addresses
     * @return void
     */
    public function testGetAllAddresses()
    {
        $this->createNewAddress();

        $response = $this->call('GET', '/v1/users/addresses');
        $data = json_decode($response->getContent());

        $this->assertEquals(200, $response->status());
        $this->assertEquals(false, $data->error);
        $this->assertEquals('Successfully get user\'s addresses.', $data->message);
        $this->assertEquals(2, count($data->addresses));
    }

    /**
     * Test get empty address
     * @return [type] [description]
     */
    public function testGetEmptyAddress()
    {
        $this->customerLogin();

        $response = $this->call('GET', '/v1/users/addresses');
        $data = json_decode($response->getContent());

        $this->assertEquals(200, $response->status());
        $this->assertEquals(false, $data->error);
        $this->assertEquals('Address empty.', $data->message);
        $this->assertEquals(0, count($data->addresses));
    }

    /**
     * Test update an address
     * @return void
     */
    public function testUpdateAddress()
    {
        $addresses = $this->getAllAddresses();
        $params = [
            'id' => $addresses[0]->id,
            'name' => 'Customer Address Updated',
            'email' => 'emailforaddress@mail.com',
            'phone' => '081337834312',
            'address' => 'Kings Landing.',
            'subdistrict_id' => 2
        ];
        $response = $this->call('POST', '/v1/users/addresses/update', $params);
        $data = json_decode($response->getContent());

        $this->assertEquals(200, $response->status());
        $this->assertEquals(false, $data->error);
        $this->assertEquals('Address has been updated.', $data->message);
        $this->seeInDatabase('addresses', $params);
    }

    /**
     * Test update non exist address
     * @return void
     */
    public function testUpdateNonExistAddress()
    {
        $this->customerLogin();
        $params = [
            'id' => 0,
            'name' => 'Customer Address Updated',
            'email' => 'emailforaddress@mail.com',
            'phone' => '081337834312',
            'address' => 'Kings Landing.',
            'subdistrict_id' => 2
        ];
        $response = $this->call('POST', '/v1/users/addresses/update', $params);
        $data = json_decode($response->getContent());

        $this->assertEquals(404, $response->status());
        $this->assertEquals(true, $data->error);
        $this->assertEquals('Address not found.', $data->message);
    }

    /**
     * Test delete an address
     * @return void
     */
    public function testDeleteAddress()
    {
        $addresses = $this->getAllAddresses();
        $params = [
            'id' => $addresses[0]->id
        ];

        $response = $this->call('POST', '/v1/users/addresses/delete', $params);
        $data = json_decode($response->getContent());

        $this->assertEquals(200, $response->status());
        $this->assertEquals(false, $data->error);
        $this->assertEquals('Address has been deleted.', $data->message);
        $this->seeInDatabase('addresses', ['id' => $addresses[0]->id, 'state' => 4]);
    }

    /**
     * Test delete non exist address
     * @return void
     */
    public function testDeleteNonExistAddress()
    {
        $addresses = $this->customerLogin();
        $params = [
            'id' => 0
        ];

        $response = $this->call('POST', '/v1/users/addresses/delete', $params);
        $data = json_decode($response->getContent());

        $this->assertEquals(404, $response->status());
        $this->assertEquals(true, $data->error);
        $this->assertEquals('Address not found.', $data->message);
    }
}