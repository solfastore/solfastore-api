<?php

class ProductControllerTest extends TestCase
{
    /**
     * Logs in a registered admin.
     *
     * @return String access_token
     */
    public function loginAdmin()
    {
        $admin = factory('App\User')->create([
            'name' => 'New Admin',
            'email' => 'admin@mail.com',
            'role' => 1,
            'state' => 1
        ]);
        $this->actingAs($admin);
        return $admin;
    }

    /**
     * Logged in customer.
     *
     * @return String access_token
     */
    public function loginUser()
    {
        $user = factory('App\User')->create([
            'name' => 'New Customer User',
            'email' => 'customer@mail.com',
            'role' => 2,
            'state' => 1
        ]);
        $this->actingAs($user);
        return $user;
    }

    /**
     * Add new categories.
     *
     * @return string
     */
    public function addCategories()
    {
        $cat1 = factory(App\Category::class)->create([
            'name' => 'Test category 1'
        ]);

        $subcat1 = factory(App\Category::class)->create([
            'parent_id' => $cat1->category_id,
            'name' => 'Test sub category 1'
        ]);

        $cat2 = factory(App\Category::class)->create([
            'name' => 'Test category 2'
        ]);

        $subcat2 = factory(App\Category::class)->create([
            'parent_id' => $cat2->category_id,
            'name' => 'Test sub category 2'
        ]);
    }

    /**
     * Get all categories.
     *
     * @return array
     */
    public function getAllCategories()
    {
        $this->addCategories();
        $response = $this->call('GET', '/v1/categories');
        $data = json_decode($response->getContent());

        return $data->categories;
    }

    public function addProduct()
    {
        $this->loginAdmin();
        $categories = $this->getAllCategories();
        $params = [
            'sku' => 'SKU-PRD-01',
            'brand_id' => '',
            'category_id' => $categories[0]->children[0]->category_id,
            'name' => 'Kemeja Casual Pria',
            'description' => 'Bingung cari produk fashion yang murah dan berkualitas? Azzurra adalah solusinya, brand fashion dengan desain original dan trendy, tampil berbeda dan percaya diri. :)',
            'weight' => 100,
            'price' => 50000,
            'stock' => 10,
            'options' => [
                'type' => 'select',
                'name' => 'Ukuran',
                'items' => [
                    [
                        'name' => 'XL',
                        'value' => 'xl',
                        'weight' => 100,
                        'price' => 55000
                    ],
                    [
                        'name' => 'L',
                        'value' => 'l',
                        'weight' => 100,
                        'price' => 50000
                    ]
                ]
            ],
            'attributes' => [
                'ukuran' => 'M-XL',
                'Bahan' => 'Jeans',
                'Warna' => 'Biru'
            ],
            'state' => 1
        ];

        $this->call('POST', '/v1/admins/products/add', $params);
    }

    public function getAllProducts()
    {
        $this->addProduct();
        $response = $this->call('GET', '/v1/products');
        $data = json_decode($response->getContent());

        return $data->products;
    }

    public function testAddProduct()
    {
        $this->loginAdmin();
        $categories = $this->getAllCategories();

        $params = [
            'sku' => 'SKU-PRD-01',
            'brand_id' => '',
            'category_id' => $categories[0]->children[0]->category_id,
            'name' => 'Kemeja Casual Pria',
            'description' => 'Bingung cari produk fashion yang murah dan berkualitas? Azzurra adalah solusinya, brand fashion dengan desain original dan trendy, tampil berbeda dan percaya diri. :)',
            'weight' => 100,
            'price' => 50000,
            'stock' => 10,
            'options' => [
                'type' => 'select',
                'name' => 'Ukuran',
                'items' => [
                    [
                        'name' => 'XL',
                        'value' => 'xl',
                        'weight' => 100,
                        'price' => 55000
                    ],
                    [
                        'name' => 'L',
                        'value' => 'l',
                        'weight' => 100,
                        'price' => 50000
                    ]
                ]
            ],
            'attributes' => [
                'ukuran' => 'M-XL',
                'Bahan' => 'Jeans',
                'Warna' => 'Biru'
            ],
            'state' => 1
        ];

        $response = $this->call('POST', '/v1/admins/products/add', $params);
        $data = json_decode($response->getContent());

        $this->assertEquals(201, $response->status());
        $this->assertEquals(false, $data->error);
        $this->assertEquals('Product has been created.', $data->message);
        $this->seeInDatabase('categories', ['name' => 'Test sub category 1']);
        $this->seeInDatabase('products', [
            'sku' => 'SKU-PRD-01',
            'name' => 'Kemeja Casual Pria',
            'category_id' => $categories[0]->children[0]->category_id,
            'description' => 'Bingung cari produk fashion yang murah dan berkualitas? Azzurra adalah solusinya, brand fashion dengan desain original dan trendy, tampil berbeda dan percaya diri. :)',
            'price' => 50000,
            'stock' => 10,
        ]);
    }

    public function testAddProductWithCustomerCredentials()
    {
        $this->loginUser();
        $categories = $this->getAllCategories();

        $params = [
            'sku' => 'SKU-PRD-01',
            'brand_id' => '',
            'category_id' => $categories[0]->children[0]->category_id,
            'name' => 'Kemeja Casual Pria',
            'description' => 'Bingung cari produk fashion yang murah dan berkualitas? Azzurra adalah solusinya, brand fashion dengan desain original dan trendy, tampil berbeda dan percaya diri. :)',
            'weight' => 100,
            'price' => 50000,
            'stock' => 10,
            'options' => [
                'type' => 'select',
                'name' => 'Ukuran',
                'items' => [
                    [
                        'name' => 'XL',
                        'value' => 'xl',
                        'weight' => 100,
                        'price' => 55000
                    ],
                    [
                        'name' => 'L',
                        'value' => 'l',
                        'weight' => 100,
                        'price' => 50000
                    ]
                ]
            ],
            'attributes' => [
                'ukuran' => 'M-XL',
                'Bahan' => 'Jeans',
                'Warna' => 'Biru'
            ],
            'state' => 1
        ];

        $response = $this->call('POST', '/v1/admins/products/add', $params);
        $data = json_decode($response->getContent());

        $this->assertArrayHasKey('error', (array)$data);
        $this->assertArrayHasKey('message', (array)$data);
        $this->assertEquals(401, $response->status());
        $this->assertEquals(true, $data->error);
        $this->assertEquals('Unauthorized access.', $data->message);
    }

    public function testAddProductWithGuestCredentials()
    {
        $categories = $this->getAllCategories();

        $params = [
            'sku' => 'SKU-PRD-01',
            'brand_id' => '',
            'category_id' => $categories[0]->children[0]->category_id,
            'name' => 'Kemeja Casual Pria',
            'description' => 'Bingung cari produk fashion yang murah dan berkualitas? Azzurra adalah solusinya, brand fashion dengan desain original dan trendy, tampil berbeda dan percaya diri. :)',
            'weight' => 100,
            'price' => 50000,
            'stock' => 10,
            'options' => [
                'type' => 'select',
                'name' => 'Ukuran',
                'items' => [
                    [
                        'name' => 'XL',
                        'value' => 'xl',
                        'weight' => 100,
                        'price' => 55000
                    ],
                    [
                        'name' => 'L',
                        'value' => 'l',
                        'weight' => 100,
                        'price' => 50000
                    ]
                ]
            ],
            'attributes' => [
                'ukuran' => 'M-XL',
                'Bahan' => 'Jeans',
                'Warna' => 'Biru'
            ],
            'state' => 1
        ];

        $response = $this->call('POST', '/v1/admins/products/add', $params);
        $data = json_decode($response->getContent());

        $this->assertArrayHasKey('error', (array)$data);
        $this->assertArrayHasKey('message', (array)$data);
        $this->assertEquals(401, $response->status());
        $this->assertEquals(true, $data->error);
        $this->assertEquals('Unauthorized.', $data->message);
    }

    public function testAddProductWithNonExistCategory()
    {
        $this->loginAdmin();

        $params = [
            'sku' => 'SKU-PRD-01',
            'brand_id' => '',
            'category_id' => 'xxx-xxx',
            'name' => 'Kemeja Casual Pria',
            'description' => 'Bingung cari produk fashion yang murah dan berkualitas? Azzurra adalah solusinya, brand fashion dengan desain original dan trendy, tampil berbeda dan percaya diri. :)',
            'weight' => 100,
            'price' => 50000,
            'stock' => 10,
            'options' => [
                'type' => 'select',
                'name' => 'Ukuran',
                'items' => [
                    [
                        'name' => 'XL',
                        'value' => 'xl',
                        'weight' => 100,
                        'price' => 55000
                    ],
                    [
                        'name' => 'L',
                        'value' => 'l',
                        'weight' => 100,
                        'price' => 50000
                    ]
                ]
            ],
            'attributes' => [
                'ukuran' => 'M-XL',
                'Bahan' => 'Jeans',
                'Warna' => 'Biru'
            ],
            'state' => 1
        ];

        $response = $this->call('POST', '/v1/admins/products/add', $params);
        $data = json_decode($response->getContent());

        $this->assertArrayHasKey('error', (array)$data);
        $this->assertArrayHasKey('message', (array)$data);
        $this->assertEquals(202, $response->status());
        $this->assertEquals(true, $data->error);
        $this->assertEquals('Category not found.', $data->message);
    }

    public function testAddProductWithNonExistBrand()
    {
        $this->loginAdmin();
        $categories = $this->getAllCategories();

        $params = [
            'sku' => 'SKU-PRD-01',
            'brand_id' => 'brand-xxx',
            'category_id' => $categories[0]->children[0]->category_id,
            'name' => 'Kemeja Casual Pria',
            'description' => 'Bingung cari produk fashion yang murah dan berkualitas? Azzurra adalah solusinya, brand fashion dengan desain original dan trendy, tampil berbeda dan percaya diri. :)',
            'weight' => 100,
            'price' => 50000,
            'stock' => 10,
            'options' => [
                'type' => 'select',
                'name' => 'Ukuran',
                'items' => [
                    [
                        'name' => 'XL',
                        'value' => 'xl',
                        'weight' => 100,
                        'price' => 55000
                    ],
                    [
                        'name' => 'L',
                        'value' => 'l',
                        'weight' => 100,
                        'price' => 50000
                    ]
                ]
            ],
            'attributes' => [
                'ukuran' => 'M-XL',
                'Bahan' => 'Jeans',
                'Warna' => 'Biru'
            ],
            'state' => 1
        ];

        $response = $this->call('POST', '/v1/admins/products/add', $params);
        $data = json_decode($response->getContent());

        $this->assertArrayHasKey('error', (array)$data);
        $this->assertArrayHasKey('message', (array)$data);
        $this->assertEquals(202, $response->status());
        $this->assertEquals(true, $data->error);
        $this->assertEquals('Brand not found.', $data->message);
    }

    public function testGetAllProduct()
    {
        $this->addProduct();
        $response = $this->call('GET', '/v1/admins/products');
        $data = json_decode($response->getContent());

        $this->assertArrayHasKey('error', (array)$data);
        $this->assertArrayHasKey('message', (array)$data);
        $this->assertArrayHasKey('products', (array)$data);
        $this->assertEquals(200, $response->status());
        $this->assertEquals(false, $data->error);
        $this->assertEquals('Successfully get all products.', $data->message);
        $this->assertEquals(1, count($data->products));
    }

    public function testGetDetailProduct()
    {
        $products = $this->getAllProducts();
        $params = [
            'product_id' => $products[0]->product_id
        ];
        $response = $this->call('POST', '/v1/admins/products/detail', $params);
        $data = json_decode($response->getContent());

        $this->assertArrayHasKey('error', (array)$data);
        $this->assertArrayHasKey('message', (array)$data);
        $this->assertArrayHasKey('product', (array)$data);
        $this->assertArrayHasKey('category', (array)$data->product);
        $this->assertEquals(200, $response->status());
        $this->assertEquals(false, $data->error);
        $this->assertEquals('Successfully get product detail.', $data->message);
    }

    public function testGetDetailNonExistProduct()
    {
        $products = $this->loginAdmin();
        $params = [
            'product_id' => 'product-xxx'
        ];
        $response = $this->call('POST', '/v1/admins/products/detail', $params);
        $data = json_decode($response->getContent());

        $this->assertArrayHasKey('error', (array)$data);
        $this->assertArrayHasKey('message', (array)$data);
        $this->assertArrayHasKey('product', (array)$data);
        $this->assertEquals(404, $response->status());
        $this->assertEquals(true, $data->error);
        $this->assertEquals('Product not found.', $data->message);
    }

    public function testSearchProductWithAllQuery()
    {
        $products = $this->getAllProducts();
        $params = [
            'all_state' => 1,
            'created_by' => $products[0]->created_by,
            'product_id' => $products[0]->product_id,
            'q' => 'Kemeja',
            'brand_id' => $products[0]->brand_id,
            'category_id' => $products[0]->category_id,
            'limit' => 1,
            'page' => 1
        ];
        $response = $this->call('POST', '/v1/admins/products/search', $params);
        $data = json_decode($response->getContent());

        $this->assertArrayHasKey('error', (array)$data);
        $this->assertArrayHasKey('message', (array)$data);
        $this->assertArrayHasKey('products', (array)$data);
        $this->assertEquals(200, $response->status());
        $this->assertEquals(false, $data->error);
        $this->assertEquals('Successfully get list of products.', $data->message);
    }

    public function testUpdateProduct()
    {
        $products = $this->getAllProducts();
        $params = [
            'product_id' => $products[0]->product_id,
            'sku' => 'SKU-PRD-01',
            'name' => 'Updated Kemeja Casual Pria',
            'description' => 'Bingung cari produk fashion yang murah dan berkualitas? Azzurra adalah solusinya, brand fashion dengan desain original dan trendy, tampil berbeda dan percaya diri. :)',
            'weight' => 100,
            'price' => 50000,
            'stock' => 10
        ];
        $response = $this->call('POST', '/v1/admins/products/update', $params);
        $data = json_decode($response->getContent());

        $this->assertArrayHasKey('error', (array)$data);
        $this->assertArrayHasKey('message', (array)$data);
        $this->assertEquals(200, $response->status());
        $this->assertEquals(false, $data->error);
        $this->assertEquals('Product has been updated.', $data->message);
        $this->seeInDatabase('products', $params);
    }

    public function testUpdateNonExistProduct()
    {
        $this->loginAdmin();
        $params = [
            'product_id' => '0',
            'sku' => 'SKU-PRD-01',
            'name' => 'Updated Kemeja Casual Pria',
            'description' => 'Bingung cari produk fashion yang murah dan berkualitas? Azzurra adalah solusinya, brand fashion dengan desain original dan trendy, tampil berbeda dan percaya diri. :)',
            'weight' => 100,
            'price' => 50000,
            'stock' => 10
        ];
        $response = $this->call('POST', '/v1/admins/products/update', $params);
        $data = json_decode($response->getContent());

        $this->assertArrayHasKey('error', (array)$data);
        $this->assertArrayHasKey('message', (array)$data);
        $this->assertEquals(404, $response->status());
        $this->assertEquals(true, $data->error);
        $this->assertEquals('Product not found.', $data->message);
    }

    public function testDeleteProduct()
    {
        $products = $this->getAllProducts();
        $params = [
            'product_id' => $products[0]->product_id
        ];
        $response = $this->call('POST', '/v1/admins/products/delete', $params);
        $data = json_decode($response->getContent());

        $this->assertArrayHasKey('error', (array)$data);
        $this->assertArrayHasKey('message', (array)$data);
        $this->assertEquals(200, $response->status());
        $this->assertEquals(false, $data->error);
        $this->assertEquals('Product has been deleted.', $data->message);
        $this->seeInDatabase('products', ['product_id' => $products[0]->product_id, 'state' => 4]);
    }

    public function testDeleteNonExistProduct()
    {
        $this->loginAdmin();
        $params = [
            'product_id' => 'product-xxx'
        ];
        $response = $this->call('POST', '/v1/admins/products/delete', $params);
        $data = json_decode($response->getContent());

        $this->assertArrayHasKey('error', (array)$data);
        $this->assertArrayHasKey('message', (array)$data);
        $this->assertEquals(404, $response->status());
        $this->assertEquals(true, $data->error);
        $this->assertEquals('Product not found.', $data->message);
    }

    public function testAddPhotoToProduct()
    {
        $products = $this->getAllProducts();
        $params = [
            'product_id' => $products[0]->product_id,
            'image_title' => 'New Image Title',
            'image_photo' => 'path/to/image/directory/image.jpg'
        ];
        $response = $this->call('POST', '/v1/admins/products/add-photo', $params);
        $data = json_decode($response->getContent());

        $this->assertArrayHasKey('error', (array)$data);
        $this->assertArrayHasKey('message', (array)$data);
        $this->assertEquals(200, $response->status());
        $this->assertEquals(false, $data->error);
        $this->assertEquals('Image has been added.', $data->message);
    }

    public function testAddPhotoToNonExistProduct()
    {
        $this->loginAdmin();
        $params = [
            'product_id' => 'product-xxx',
            'image_title' => 'New Image Title',
            'image_photo' => 'path/to/image/directory/image.jpg'
        ];
        $response = $this->call('POST', '/v1/admins/products/add-photo', $params);
        $data = json_decode($response->getContent());

        $this->assertArrayHasKey('error', (array)$data);
        $this->assertArrayHasKey('message', (array)$data);
        $this->assertEquals(404, $response->status());
        $this->assertEquals(true, $data->error);
        $this->assertEquals('Product not found.', $data->message);
    }

    public function testDeletePhotoFromProduct()
    {
        $products = $this->getAllProducts();

        $params = [
            'product_id' => $products[0]->product_id,
            'image_title' => 'New Image Title',
            'image_photo' => 'path/to/image/directory/image.jpg'
        ];
        $this->call('POST', '/v1/admins/products/add-photo', $params);

        $response = $this->call('GET', '/v1/products');
        $data = json_decode($response->getContent());

        $params = [
            'product_id' => $data->products[0]->product_id,
            'image_uuid' => $data->products[0]->images[0]->uuid
        ];
        $response = $this->call('POST', '/v1/admins/products/delete-photo', $params);
        $data = json_decode($response->getContent());

        $this->assertArrayHasKey('error', (array)$data);
        $this->assertArrayHasKey('message', (array)$data);
        $this->assertEquals(200, $response->status());
        $this->assertEquals(false, $data->error);
        $this->assertEquals('Image has been removed.', $data->message);
    }

    public function testDeletePhotoFromNonExistProduct()
    {
        $this->loginAdmin();
        $params = [
            'product_id' => 'some-product-uuid',
            'image_uuid' => 'some-image-uuid'
        ];
        $response = $this->call('POST', '/v1/admins/products/delete-photo', $params);
        $data = json_decode($response->getContent());

        $this->assertArrayHasKey('error', (array)$data);
        $this->assertArrayHasKey('message', (array)$data);
        $this->assertEquals(404, $response->status());
        $this->assertEquals(true, $data->error);
        $this->assertEquals('Product not found.', $data->message);
    }

    public function testDeletePhotoFromProductWithNoPhoto()
    {
        $products = $this->getAllProducts();
        $params = [
            'product_id' => $products[0]->product_id,
            'image_uuid' => 'just-some-image-uuid'
        ];
        $response = $this->call('POST', '/v1/admins/products/delete-photo', $params);
        $data = json_decode($response->getContent());

        $this->assertArrayHasKey('error', (array)$data);
        $this->assertArrayHasKey('message', (array)$data);
        $this->assertEquals(404, $response->status());
        $this->assertEquals(true, $data->error);
        $this->assertEquals('Image not found. Wrong Uuid.', $data->message);
    }
}