<?php

class BrandControllerTest extends TestCase
{
    /**
     * Register a new admin.
     *
     * @return void
     */
    public function registerAdmin()
    {
        DB::table('users')->insert([
            'name' => 'New Admin',
            'email' => 'admin@mail.com',
            'password' => app('hash')->make('password'),
            'role' => 1,
            'state' => 1,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ]);
    }

    /**
     * Logs in a registered admin.
     *
     * @return String access_token
     */
    public function login()
    {
        $this->registerAdmin();
        $params = [
            'grant_type' => 'password',
            'client_id' => 2,
            'client_secret' => 'jtENSd6fEJ5k9PFoGHTfWHc5Kqn8i8IzG4fp1DHm',
            'username' => 'admin@mail.com',
            'password' => 'password',
        ];
        $response = $this->call('POST', '/v1/oauth/token', $params);
        $data = json_decode($response->getContent());
        return $data->access_token;
    }

    /**
     * Add new brands.
     *
     * @return string
     */
    public function addBrands()
    {
        $token = $this->login();
        $server = $this->transformHeadersToServerVars(['Authorization' => 'Bearer ' . $token]);

        $params1 = [
            'name' => 'Test Brand 1',
            'description' => 'A test brand.',
            'logo_url' => 'http://placehold.it/350x350',
        ];

        $params2 = [
            'name' => 'Test Brand 2',
            'description' => 'A test brand.',
            'logo_url' => 'http://placehold.it/350x350',
        ];

        $this->call('POST', '/v1/admins/brands/add', $params1, [], [], $server);
        $this->call('POST', '/v1/admins/brands/add', $params2, [], [], $server);

        return $token;
    }

    /**
     * Search for a brand.
     *
     * @return void
     */
    public function searchBrand()
    {
        $token = $this->addBrands();
        $server = $this->transformHeadersToServerVars(['Authorization' => 'Bearer ' . $token]);

        $params = [
            'q' => 'Test Brand 1',
        ];

        $response = $this->call('POST', '/v1/admins/brands/search', $params, [], [], $server);
        $data = json_decode($response->getContent());
        $result['token'] = $token;
        $result['brands'] = $data->brands;

        return $result;
    }

    /**
     * Test adding a new brand.
     *
     * @return void
     */
    public function testAddBrand()
    {
        $token = $this->login();
        $server = $this->transformHeadersToServerVars(['Authorization' => 'Bearer ' . $token]);
        $params = [
            'name' => 'Test Brand',
            'description' => 'A test brand.',
            'logo_url' => 'http://placehold.it/350x350'
        ];

        $response = $this->call('POST', '/v1/admins/brands/add', $params, [], [], $server);
        $data = json_decode($response->getContent());

        $this->assertEquals(201, $response->status());
        $this->assertEquals(false, $data->error);
        $this->assertEquals('Brand has been created.', $data->message);
        $this->seeInDatabase('brands', ['name' => 'Test Brand', 'slug' => 'test-brand', 'description' => 'A test brand.']);
    }

    /**
     * Test adding brand with existing slug.
     *
     * @return void
     */
    public function testAddBrandWithExistingSlug()
    {
        $token = $this->addBrands();
        $server = $this->transformHeadersToServerVars(['Authorization' => 'Bearer ' . $token]);
        $params = [
            'name' => 'Test Brand 1',
            'slug' => 'test-brand-1',
            'description' => 'Test adding brand with existing slug.',
            'logo_url' => 'http://placehold.it/350x350'
        ];

        $response = $this->call('POST', '/v1/admins/brands/add', $params, [], [], $server);
        $data = json_decode($response->getContent());

        $this->assertEquals(409, $response->status());
        $this->assertEquals(true, $data->error);
        $this->assertEquals('Brand\'s slug already exist.', $data->message);
    }

    /**
     * Test creating a new brand with wrong token.
     *
     * @return void
     */
    public function testAddBrandWithWrongToken()
    {
        $token = $this->login();
        $server = $this->transformHeadersToServerVars(['Authorization' => 'Bearer ' . $token . 'invalid']);

        $params = [
            'name' => 'Test Brand',
            'description' => 'A test brand.',
            'logo_url' => 'http://placehold.it/350x350',
        ];

        $response = $this->call('POST', '/v1/admins/brands/add', $params, [], [], $server);
        $data = json_decode($response->getContent());

        $this->assertEquals(401, $response->status());
        $this->assertArrayHasKey('error', (array)$data);
        $this->assertArrayHasKey('message', (array)$data);
        $this->assertEquals(true, $data->error);
        $this->assertEquals('Unauthorized.', $data->message);
    }

    /**
     * Test creating a new brand without token.
     *
     * @return void
     */
    public function testAddBrandWithoutToken()
    {
        $params = [
            'name' => 'Test Brand',
            'description' => 'A test brand.',
            'logo_url' => 'http://placehold.it/350x350',
        ];

        $response = $this->call('POST', '/v1/admins/brands/add', $params);
        $data = json_decode($response->getContent());

        $this->assertEquals(401, $response->status());
        $this->assertArrayHasKey('error', (array)$data);
        $this->assertArrayHasKey('message', (array)$data);
        $this->assertEquals(true, $data->error);
        $this->assertEquals('Unauthorized.', $data->message);
    }

    /**
     * Test creating a new brand by non admin user.
     *
     * @return void
     */
    public function testCustomerAddBrand()
    {
        $user = factory('App\User')->create([
            'role' => 2,
            'state' => 1
        ]);
        $this->actingAs($user);

        $params = [
            'name' => 'Test Brand',
            'description' => 'A test brand.',
            'logo_url' => 'http://placehold.it/350x350',
        ];

        $response = $this->call('POST', '/v1/admins/brands/add', $params);
        $data = json_decode($response->getContent());

        $this->assertEquals(401, $response->status());
        $this->assertArrayHasKey('error', (array)$data);
        $this->assertArrayHasKey('message', (array)$data);
        $this->assertEquals(true, $data->error);
        $this->assertEquals('Unauthorized access.', $data->message);
    }

    /**
     * Test get all brands.
     *
     * @return int
     */
    public function testGetAllBrands()
    {
        $token = $this->addBrands();
        $server = $this->transformHeadersToServerVars(['Authorization' => 'Bearer ' . $token]);

        $response = $this->call('GET', '/v1/admins/brands', [], [], [], $server);
        $data = json_decode($response->getContent());

        $this->assertEquals(200, $response->status());
        $this->assertEquals(false, $data->error);
        $this->assertEquals('Successfully get all brands.', $data->message);
        $this->assertEquals(2, count($data->brands));
    }

    /**
     * Test get brand by brand ID.
     *
     * @return void
     */
    public function testGetBrandById()
    {
        $params = $this->searchBrand();
        $server = $this->transformHeadersToServerVars(['Authorization' => 'Bearer ' . $params['token']]);

        $params = [
            'brand_id' => $params['brands'][0]->brand_id,
        ];

        $response = $this->call('POST', '/v1/admins/brands/search', $params, [], [], $server);
        $data = json_decode($response->getContent());

        $this->assertEquals(200, $response->status());
        $this->assertEquals(false, $data->error);
        $this->assertEquals('Successfully get list of brands.', $data->message);
        $this->assertEquals(1, count($data->brands));
    }

    /**
     * Test searching a brand.
     *
     * @return void
     */
    public function testSearchBrand()
    {
        $token = $this->addBrands();
        $server = $this->transformHeadersToServerVars(['Authorization' => 'Bearer ' . $token]);

        $params = [
            'q' => 'brand 1',
        ];

        $response = $this->call('POST', '/v1/admins/brands/search', $params, [], [], $server);
        $data = json_decode($response->getContent());

        $this->assertEquals(200, $response->status());
        $this->assertEquals(false, $data->error);
        $this->assertEquals('Successfully get list of brands.', $data->message);
        $this->assertEquals(1, count($data->brands));
    }

    /**
     * Test searching a brand with limit.
     *
     * @return void
     */
    public function testSearchBrandWithLimit()
    {
        $token = $this->addBrands();
        $server = $this->transformHeadersToServerVars(['Authorization' => 'Bearer ' . $token]);

        $params = [
            'q' => 'test',
            'limit' => 1,
        ];

        $response = $this->call('POST', '/v1/admins/brands/search', $params, [], [], $server);
        $data = json_decode($response->getContent());

        $this->assertEquals(200, $response->status());
        $this->assertEquals(false, $data->error);
        $this->assertEquals('Successfully get list of brands.', $data->message);
        $this->assertEquals(1, count($data->brands));
    }

    /**
     * Test updating a brand.
     *
     * @return void
     */
    public function testUpdateBrand()
    {
        $data = $this->searchBrand();
        $server = $this->transformHeadersToServerVars(['Authorization' => 'Bearer ' . $data['token']]);

        $params = [
            'brand_id' => $data['brands'][0]->brand_id,
            'name' => 'Test Update Brand',
            'description' => 'A test brand updated.',
            'logo_url' => 'http://placehold.it/400x400',
        ];

        $response = $this->call('POST', '/v1/admins/brands/update', $params, [], [], $server);
        $data = json_decode($response->getContent());

        $this->assertEquals(200, $response->status());
        $this->assertEquals(false, $data->error);
        $this->assertEquals('Brand has been updated.', $data->message);
        $this->seeInDatabase('brands', ['name' => 'Test Update Brand', 'description' => 'A test brand updated.']);
    }

    /**
     * Test updating a brand with wrong ID.
     *
     * @return void
     */
    public function testUpdateBrandWithWrongId()
    {
        $data = $this->searchBrand();
        $server = $this->transformHeadersToServerVars(['Authorization' => 'Bearer ' . $data['token']]);

        $params = [
            'id' => 0,
            'name' => 'Test Update Brand',
        ];

        $response = $this->call('POST', '/v1/admins/brands/update', $params, [], [], $server);
        $data = json_decode($response->getContent());

        $this->assertEquals(404, $response->status());
        $this->assertEquals(true, $data->error);
        $this->assertEquals('Brand not found.', $data->message);
    }

    /**
     * Test deleting a brand.
     *
     * @return void
     */
    public function testDeleteBrand()
    {
        $params = $this->searchBrand();
        $brandId = $params['brands'][0]->brand_id;
        $server = $this->transformHeadersToServerVars(['Authorization' => 'Bearer ' . $params['token']]);

        $params = [
            'brand_id' => $brandId,
        ];

        $response = $this->call('POST', '/v1/admins/brands/delete', $params, [], [], $server);
        $data = json_decode($response->getContent());

        $this->assertEquals(200, $response->status());
        $this->assertEquals(false, $data->error);
        $this->assertEquals('Brand has been deleted.', $data->message);
        $this->seeInDatabase('brands', ['brand_id' => $brandId, 'state' => 4]);
    }

    /**
     * Test deleting a brand with wrong ID.
     *
     * @return void
     */
    public function testDeleteBrandWithWrongId()
    {
        $token = $this->login();
        $server = $this->transformHeadersToServerVars(['Authorization' => 'Bearer ' . $token]);

        $params = [
            'brand_id' => 0
        ];

        $response = $this->call('POST', '/v1/admins/brands/delete', $params, [], [], $server);
        $data = json_decode($response->getContent());

        $this->assertEquals(404, $response->status());
        $this->assertEquals(true, $data->error);
        $this->assertEquals('Brand not found.', $data->message);
    }
}