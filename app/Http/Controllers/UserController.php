<?php
namespace App\Http\Controllers;

use App\Http\Utilities\Constants;
use Illuminate\Http\Request;
use Validator;
use App\User;

class UserController extends Controller
{
    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email|unique:users',
            'password' => 'required'
        ]);
        if ($validator->fails()) {
            $response['error'] = true;
            $response['message'] = 'Validation error.';
            $response['validation'] = $validator->errors();
            return response($response, Constants::HTTP_ERROR_BAD_REQUEST);
        }
        //create user
        $user = new User;
        $user->fill($request->all());
        if($user->save()) {
            $response['error'] = false;
            $response['message'] = 'User has been created.';
            return response($response, Constants::HTTP_SUCCESS_CREATED);
        } else {
            $response['error'] = true;
            $response['message'] = 'Error when trying to create user.';
            return response($response, Constants::HTTP_SUCCESS_ACCEPTED);
        }
    }

    public function profile(Request $request)
    {
        $user = $this->getAuthorizedUser($request);
        $response['error'] = false;
        $response['message'] = 'Successfully get user profile.';
        $response['user'] = $user;
        $response['user']['addresses'] = $user->addresses;
        return response($response, Constants::HTTP_SUCCESS_OK);
    }

    public function update(Request $request)
    {
        $user = $this->getAuthorizedUser($request);
        $user->fill($request->only(['name', 'password']));
        $user->save();
        $response['error'] = false;
        $response['message'] = 'User profile has been updated.';
        return response($response, Constants::HTTP_SUCCESS_OK);
    }
}
