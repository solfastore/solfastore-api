<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Laravel\Lumen\Routing\Controller as BaseController;
use Auth;

class Controller extends BaseController
{
    protected $user;

    protected function getAuthorizedUser(Request $request)
    {
        $this->user = Auth::user();
        return $this->user;
    }
}
