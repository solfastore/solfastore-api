<?php

namespace App\Http\Controllers;

use App\Http\Utilities\Constants;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Cart;
use App\Product;

class CartController extends Controller
{
    public function index(Request $request)
    {
        $user = $this->getAuthorizedUser($request);
        $carts = Cart::with('products', 'user')->where('state', Constants::STATE_ACTIVE)->where('user_id', $user->id)->where('expired_at', '>', Carbon::now())->first();

        $response['error'] = false;
        if (empty($carts)) {
            $response['message'] = 'Carts empty.';
        } else {
            if (!$carts->products->isEmpty()) {
                $response['message'] = 'Successfully get user\'s cart.';
            } else {
                $response['message'] = 'Carts empty.';
            }
        }
        $response['carts'] = $carts;
        return response($response, Constants::HTTP_SUCCESS_OK);
    }

    public function create(Request $request)
    {
        $user = $this->getAuthorizedUser($request);
        $productId = $request->input('product_id');
        $amount = $request->input('amount');

        $product = Product::find($productId);
        if (empty($product)) {
            $response['error'] = true;
            $response['message'] = 'Product not found.';
            return response($response, Constants::HTTP_ERROR_NOT_FOUND);
        }
        if ($product->stock <= 0 || $product->stock < $amount) {
            $response['error'] = true;
            $response['message'] = 'Not enough stock.';
            return response($response, Constants::HTTP_ERROR_NOT_FOUND);
        }

        $cart = Cart::where('state', Constants::STATE_ACTIVE)->where('user_id', $user->id)->where('expired_at', '>', Carbon::now())->first();
        if (empty($cart)) {
            $cart = new Cart;
            $cart->fill(['user_id' => $user->id, 'expired_at' => Carbon::now()->addHours(24)]);
            $cart->save();
        }

        $productInCart = $cart->products()->where('carts_products.product_id', $productId)->first();
        if (!empty($productInCart)) {
            $amount = $amount + $productInCart->pivot->amount;
            if ($product->stock < $amount) {
                $response['error'] = true;
                $response['message'] = 'Not enough stock.';
                return response($response, Constants::HTTP_ERROR_NOT_FOUND);
            }
        }

        $productAdded = $cart->products()->syncWithoutDetaching([$productId => ['amount' => $amount, 'price' => $product->price]]);

        $response['error'] = false;
        $response['message'] = 'Produk telah ditambahkan ke keranjang belanja.';
        return response($response, Constants::HTTP_SUCCESS_CREATED);
    }

    public function update(Request $request)
    {
        $user = $this->getAuthorizedUser($request);
        $productId = $request->input('product_id');
        $amount = $request->input('amount');

        $product = Product::find($productId);
        if (empty($product)) {
            $response['error'] = true;
            $response['message'] = 'Produk tidak ditemukan.';
            return response($response, Constants::HTTP_ERROR_NOT_FOUND);
        }
        if ($product->stock <= 0 || $product->stock < $amount) {
            $response['error'] = true;
            $response['message'] = 'Stok produk tidak cukup.';
            return response($response, Constants::HTTP_ERROR_NOT_FOUND);
        }

        $cart = Cart::where('state', Constants::STATE_ACTIVE)->where('user_id', $user->id)->where('expired_at', '>', Carbon::now())->first();
        if (empty($cart)) {
            $cart = new Cart;
            $cart->fill(['user_id' => $user->id, 'expired_at' => Carbon::now()->addHours(24)]);
            $cart->save();
        }

        $productInCart = $cart->products()->where('carts_products.product_id', $productId)->exists();
        if (!$productInCart) {
            $response['error'] = true;
            $response['message'] = 'Produk tidak ada di keranjang belanja.';
            return response($response, Constants::HTTP_ERROR_NOT_FOUND);
        }
        if ($amount == 0) {
            $productRemoved = $cart->products()->detach($productId);
            $response['error'] = false;
            $response['message'] = 'Produk telah dihapus dari keranjang belanja.';
            return response($response, Constants::HTTP_SUCCESS_ACCEPTED);
        }

        $productUpdated = $cart->products()->updateExistingPivot($productId, ['amount' => $amount, 'price' => $product->price]);

        $response['error'] = false;
        $response['message'] = 'Keranjang belanja berhasil diperbarui.';
        return response($response, Constants::HTTP_SUCCESS_CREATED);
    }

    public function delete(Request $request)
    {
        $user = $this->getAuthorizedUser($request);
        $productId = $request->input('product_id');

        $product = Product::find($productId);
        if (empty($product)) {
            $response['error'] = true;
            $response['message'] = 'Produk tidak ditemukan.';
            return response($response, Constants::HTTP_ERROR_NOT_FOUND);
        }

        $cart = Cart::where('state', Constants::STATE_ACTIVE)->where('user_id', $user->id)->where('expired_at', '>', Carbon::now())->first();
        if (empty($cart)) {
            $response['error'] = true;
            $response['message'] = 'Keranjang belanja kosong.';
            return response($response, Constants::HTTP_ERROR_NOT_FOUND);
        }

        $productInCart = $cart->products()->where('carts_products.product_id', $productId)->exists();
        if (!$productInCart) {
            $response['error'] = true;
            $response['message'] = 'Produk tidak ada di keranjang belanja.';
            return response($response, Constants::HTTP_ERROR_NOT_FOUND);
        }

        $cart->products()->detach($productId);
        $response['error'] = false;
        $response['message'] = 'Produk telah dihapus dari kerangjang belanja.';
        return response($response, Constants::HTTP_SUCCESS_CREATED);
    }

}
