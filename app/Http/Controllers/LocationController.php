<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class LocationController extends Controller
{
    public function provinces(Request $request)
    {
        $provinces = DB::table('province')->orderBy('name')->get();
        if ($provinces->isEmpty()) {
            $response['error'] = true;
            $response['message'] = 'Result empty.';
            return $response;
        }
        $response['error'] = true;
        $response['message'] = 'Successfully get list of provinces.';
        $response['provinces'] = $provinces;
        return $response;
    }

    public function cities(Request $request)
    {
        $query = DB::table('city');
        if ($request->has('province_id')) {
            $query->where('province_id', $request->input('province_id'));
        }
        $cities = $query->orderBy('name')->get();

        if ($cities->isEmpty()) {
            $response['error'] = true;
            $response['message'] = 'Result empty.';
            return $response;
        }
        $response['error'] = true;
        $response['message'] = 'Successfully get list of cities.';
        $response['cities'] = $cities;
        return $response;
    }

    public function districts(Request $request)
    {
        $query = DB::table('district');
        if ($request->has('city_id')) {
            $query->where('city_id', $request->input('city_id'));
        }
        $districts = $query->orderBy('name')->get();

        if ($districts->isEmpty()) {
            $response['error'] = true;
            $response['message'] = 'Result empty.';
            return $response;
        }
        $response['error'] = true;
        $response['message'] = 'Successfully get list of districts.';
        $response['districts'] = $districts;
        return $response;
    }

    public function subdistricts(Request $request)
    {
        $query = DB::table('subdistrict');
        if ($request->has('district_id')) {
            $query->where('district_id', $request->input('district_id'));
        } else {
            $limit = ($request->has('limit')) ? (int)$request->input('limit') : 100;
            $query->limit($limit);
        }
        $subdistrict = $query->orderBy('name')->get();

        if ($subdistrict->isEmpty()) {
            $response['error'] = true;
            $response['message'] = 'Result empty.';
            return $response;
        }
        $response['error'] = true;
        $response['message'] = 'Successfully get list of subdistrict.';
        $response['subdistrict'] = $subdistrict;
        return $response;
    }
}