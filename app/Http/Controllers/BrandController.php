<?php

namespace App\Http\Controllers;

use App\Http\Utilities\Constants;
use Illuminate\Http\Request;
use App\Brand;

class BrandController extends Controller
{
    public function index(Request $request)
    {
        $brands = Brand::where('state', Constants::STATE_ACTIVE)->orderBy('name')->get();
        $response['error'] = false;
        $response['message'] = 'Successfully get all brands.';
        $response['brands'] = $brands;
        return response($response, Constants::HTTP_SUCCESS_OK);
    }

    public function search(Request $request)
    {
        $query = Brand::query();
        $query->where('state', Constants::STATE_ACTIVE);
        if ($request->has('q')) {
            $query = $query->where('name', 'LIKE', '%' . $request->input('q') . '%');
        }
        if ($request->has('brand_id')) {
            $query = $query->where('brand_id', $request->input('brand_id'));
        }
        if ($request->has('limit')) {
            $query = $query->limit($request->input('limit'));
        }
        $query->orderBy('name');
        $brands = $query->get();
        $response['error'] = false;
        $response['message'] = 'Successfully get list of brands.';
        $response['brands'] = $brands;
        return response($response, Constants::HTTP_SUCCESS_OK);
    }

    public function create(Request $request)
    {
        $brandExist = Brand::where('slug', $request->input('slug'))->first();
        if ($brandExist) {
            $response['error'] = true;
            $response['message'] = 'Brand\'s slug already exist.';
            return response($response, Constants::HTTP_ERROR_CONFLICT);
        }
        //create brand
        $brand = new Brand;
        $brand->fill($request->all());
        if (!$brand->save()) {
            $response['error'] = true;
            $response['message'] = 'Error when trying to create brand.';
            return response($response, Constants::HTTP_SUCCESS_ACCEPTED);
        } else {
            $response['error'] = false;
            $response['message'] = 'Brand has been created.';
            return response($response, Constants::HTTP_SUCCESS_CREATED);
        }
    }

    public function update(Request $request)
    {
        $brand = Brand::where('state', Constants::STATE_ACTIVE)->find($request->input('brand_id'));
        if (!$brand) {
            $response['error'] = true;
            $response['message'] = 'Brand not found.';
            return response($response, Constants::HTTP_ERROR_NOT_FOUND);
        }
        $brand->fill($request->all());
        if (!$brand->save()) {
            $response['error'] = true;
            $response['message'] = 'Error when trying to update brand.';
            return response($response, Constants::HTTP_SUCCESS_ACCEPTED);
        }
        $response['error'] = false;
        $response['message'] = 'Brand has been updated.';
        return response($response, Constants::HTTP_SUCCESS_OK);
    }

    public function delete(Request $request)
    {
        $brand = Brand::where('state', '!=', Constants::STATE_DELETED)->find($request->input('brand_id'));
        if (!$brand) {
            $response['error'] = true;
            $response['message'] = 'Brand not found.';
            return response($response, Constants::HTTP_ERROR_NOT_FOUND);
        }
        $brand->state = Constants::STATE_DELETED;
        if (!$brand->save()) {
            $response['error'] = true;
            $response['message'] = 'Error when trying to delete brand.';
            return response($response, Constants::HTTP_SUCCESS_ACCEPTED);
        }
        $response['error'] = false;
        $response['message'] = 'Brand has been deleted.';
        return response($response, Constants::HTTP_SUCCESS_OK);
    }
}
