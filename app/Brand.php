<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class Brand extends Model
{
    use Uuids;

    protected $primaryKey = 'brand_id';
    public $incrementing = false;

    protected $fillable = [
        'name', 'slug', 'description', 'logo_url'
    ];

    /**
     * Create the slug from the name
     */
     public function setNameAttribute($value)
    {
        $this->attributes['name'] = $value;
        $this->attributes['slug'] = str_slug($value);
    }
}