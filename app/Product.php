<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use Uuids;

    protected $primaryKey = 'product_id';
    public $incrementing = false;

    protected $guarded = [
        'product_id'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'options' => 'array',
        'attributes' => 'array',
        'images' => 'array',
    ];

    public function category()
    {
        return $this->belongsTo('App\Category', 'category_id');
    }
}