<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use Uuids;

    protected $primaryKey = 'category_id';
    public $incrementing = false;

    protected $fillable = [
        'parent_id', 'name', 'slug', 'description', 'logo_url', 'state'
    ];

    public function products()
    {
        return $this->hasMany('App\Product', 'category_id');
    }

    public function parent()
    {
        return $this->belongsTo('App\Category', 'parent_id');
    }

    public function children()
    {
        return $this->hasMany('App\Category', 'parent_id');
    }

    /**
     * Create the slug from the name
     */
     public function setNameAttribute($value)
    {
        $this->attributes['name'] = $value;
        $this->attributes['slug'] = str_slug($value);
    }
}