<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class Checkout extends Model
{
    protected $fillable = ['cart_id', 'user_id', 'address_id', 'invoice_no', 'delivery_fee', 'note', 'checkout_state'];

    public function address()
    {
        return $this->belongsTo('App\Address');
    }

    public function cart()
    {
        return $this->belongsTo('App\Cart')->with('products');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}