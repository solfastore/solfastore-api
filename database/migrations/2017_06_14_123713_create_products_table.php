<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function(Blueprint $table){
            $table->Uuid('product_id');
            $table->primary('product_id');
            $table->string('sku', 20)->nullable();
            $table->char('brand_id', 36)->nullable();
            $table->char('category_id', 36);
            $table->string('name');
            $table->longText('description');
            $table->integer('weight');
            $table->double('price', 19, 2);
            $table->integer('stock');
            $table->binary('options');
            $table->binary('attributes');
            $table->binary('images');
            $table->smallInteger('state')->default(1);
            $table->integer('created_by');
            $table->timestamps();

            $table->foreign('category_id')
                ->references('category_id')->on('categories')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('products');
    }
}
