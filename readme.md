# SolfaGaming API

[![build status](https://gitlab.com/solfagaming/solfagaming-api/badges/development/build.svg)](https://gitlab.com/solfagaming/solfagaming-api/commits/development)
[![coverage report](https://gitlab.com/solfagaming/solfagaming-api/badges/development/coverage.svg)](https://gitlab.com/solfagaming/solfagaming-api/commits/development)

SolfaGaming API Version 1, build based on [Lumen Framework](http://lumen.laravel.com/).

## Build and Maintained By:
#### Farizal Tri Anugrah - @omdjin
