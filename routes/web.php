<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$app->get('/', function () use ($app) {
    return 'Solfagaming API';
    #return $app->version();
});

$app->group(['prefix' => 'v1'], function () use ($app) {
    Dusterio\LumenPassport\LumenPassport::routes($app);
    /* public endpoint */
    $app->get('categories', 'CategoryController@index');
    $app->group(['prefix' => 'locations'], function () use ($app) {
        $app->get('provinces', ['uses' => 'LocationController@provinces']);
        $app->get('cities', ['uses' => 'LocationController@cities']);
        $app->get('districts', ['uses' => 'LocationController@districts']);
        $app->get('subdistricts', ['uses' => 'LocationController@subdistricts']);
    });
    $app->group(['prefix' => 'products'], function () use ($app) {
        $app->get('/', ['uses' => 'ProductController@index']);
        $app->post('detail', ['uses' => 'ProductController@detail']);
        $app->post('search', ['uses' => 'ProductController@search']);
    });

    /* user endpoint */
    $app->group(['prefix' => 'users'], function () use ($app) {
        $app->post('register', 'UserController@register');
        $app->group(['prefix' => 'addresses'], function () use ($app) {
            $app->get('/', ['middleware' => 'auth:api', 'uses' => 'AddressController@index']);
            $app->post('add', ['middleware' => 'auth:api', 'uses' => 'AddressController@create']);
            $app->post('update', ['middleware' => 'auth:api', 'uses' => 'AddressController@update']);
            $app->post('delete', ['middleware' => 'auth:api', 'uses' => 'AddressController@delete']);
        });
        $app->group(['prefix' => 'carts'], function () use ($app) {
            $app->get('/', ['middleware' => 'auth:api', 'uses' => 'CartController@index']);
            $app->post('add', ['middleware' => 'auth:api', 'uses' => 'CartController@create']);
            $app->post('update', ['middleware' => 'auth:api', 'uses' => 'CartController@update']);
            $app->post('delete', ['middleware' => 'auth:api', 'uses' => 'CartController@delete']);
        });
        $app->group(['prefix' => 'checkouts'], function () use ($app) {
            $app->get('/', ['middleware' => 'auth:api', 'uses' => 'CheckoutController@index']);
            $app->post('add', ['middleware' => 'auth:api', 'uses' => 'CheckoutController@create']);
            $app->post('update', ['middleware' => 'auth:api', 'uses' => 'CheckoutController@update']);
        });
        $app->group(['prefix' => 'profile'], function () use ($app) {
            $app->get('/', ['middleware' => 'auth:api', 'uses' => 'UserController@profile']);
            $app->post('update', ['middleware' => 'auth:api', 'uses' => 'UserController@update']);
        });
    });

    /* admin endpoint */
    $app->group(['prefix' => 'admins'], function () use ($app) {
        $app->group(['prefix' => 'brands'], function () use ($app) {
            $app->get('/', ['middleware' => ['auth:api', 'admin'], 'uses' => 'BrandController@index']);
            $app->post('search', ['middleware' => ['auth:api', 'admin'], 'uses' => 'BrandController@search']);
            $app->post('add', ['middleware' => ['auth:api', 'admin'], 'uses' => 'BrandController@create']);
            $app->post('update', ['middleware' => ['auth:api', 'admin'], 'uses' => 'BrandController@update']);
            $app->post('delete', ['middleware' => ['auth:api', 'admin'], 'uses' => 'BrandController@delete']);
        });
        $app->group(['prefix' => 'categories'], function () use ($app) {
            $app->get('/', ['middleware' => ['auth:api', 'admin'], 'uses' => 'CategoryController@index']);
            $app->post('search', ['middleware' => ['auth:api', 'admin'], 'uses' => 'CategoryController@search']);
            $app->post('add', ['middleware' => ['auth:api', 'admin'], 'uses' => 'CategoryController@create']);
            $app->post('update', ['middleware' => ['auth:api', 'admin'], 'uses' => 'CategoryController@update']);
            $app->post('delete', ['middleware' => ['auth:api', 'admin'], 'uses' => 'CategoryController@delete']);
        });
        $app->group(['prefix' => 'products'], function () use ($app) {
            $app->get('/', ['middleware' => ['auth:api', 'admin'], 'uses' => 'ProductController@index']);
            $app->post('detail', ['middleware' => ['auth:api', 'admin'], 'uses' => 'ProductController@detail']);
            $app->post('search', ['middleware' => ['auth:api', 'admin'], 'uses' => 'ProductController@search']);
            $app->post('add', ['middleware' => ['auth:api', 'admin'], 'uses' => 'ProductController@create']);
            $app->post('update', ['middleware' => ['auth:api', 'admin'], 'uses' => 'ProductController@update']);
            $app->post('delete', ['middleware' => ['auth:api', 'admin'], 'uses' => 'ProductController@delete']);
            $app->post('add-photo', ['middleware' => ['auth:api', 'admin'], 'uses' => 'ProductController@addPhoto']);
            $app->post('delete-photo', ['middleware' => ['auth:api', 'admin'], 'uses' => 'ProductController@deletePhoto']);
        });
    });
});